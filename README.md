polymerize
==========

A playground for polymer elements


Polymerize is project that will help developers create, reuse, and search polymer elements at one place.

Features
========
1. Create new element.
2. Search for other elements.
3. Reuse elements.
4. Versioning elements.
5. Open calatouge to search for elements.
6. Online IDE
7. GitHub Integration (maybe)

Making this project as opensource, any code, patches, ideas are welcome...
