import webapp2
import os
import logging

import sys
sys.path.insert(0, 'lib')

import simplejson as json

from polymerize.handler import Handler
from polymerize.db import CodeSnap, Component, Reuseable
import polymerize.util as util

from google.appengine.api import memcache

class Snap(Handler):
    def make_random_string(self,length=5):
        random_key = util.makeSalt(length)
        cs  = CodeSnap.get_by_key_name(random_key)
        if(cs):
            return self.make_random_string(length)
        return random_key
    def make_run_id(self,length=5):
        random_id = util.makeSalt(length)
        if(memcache.get('%s:snap:index'%random_id)):
            return self.make_run_id(length)
        return random_id
    def post(self):
        data = json.loads(self.request.body)
        data_map = {
            "comp_id" : data.get('comp_id') ,
            "el_name" : data.get('name'),
            "ref_id" : data.get('ref_id'),
            "poly_depend" : data.get('poly_depend'),
            "attr": data.get('attr'),
            "extend" : data.get('extend'),
            "index" : data.get('index'),
            "shadow" : data.get('shadow'),
            "style" : data.get('style'), 
            "script" : data.get('script')
        }
        data_map["comp_id"] = data_map["comp_id"] or 0
        if(data_map["comp_id"] and data_map["comp_id"].isdigit()):
            data_map["comp_id"] = int(data_map["comp_id"])
            c = Component.get_by_id(int(data_map["comp_id"]))
            data_map["el_name"] = c.element_name
        data_map["el_name"] = data_map["el_name"] or ''
        data_map["ref_id"] = data_map["ref_id"] or ''
        logging.error(data_map["ref_id"])
        random_key = self.make_random_string(6)
        
        c_s = CodeSnap(
                key_name = random_key,
                componentid = data_map["comp_id"],
                refrenceid = data_map["ref_id"],
                element_name = data_map["el_name"],
                element_extends = data_map["extend"],
                element_attr = data_map["attr"],
                polymer_imports = map(long,data_map["poly_depend"]),
                polymer_deep = util.deep_dependencies(map(long,data_map["poly_depend"])),
                polymer_shadow = data_map["shadow"],
                polymer_style = data_map["style"],
                polymer_script = data_map["script"],
                index_body = data_map["index"]
            )
        c_s.put()
        self.response.headers.add_header("Access-Control-Allow-Origin","http://app.polymer-ize.appspot.com:8080")
        self.write(json.dumps({
            "link":"http://snap.polymer-ize.appspot.com:8082/%s"%random_key
            }))
    def get(self):
        all_reuseable = Reuseable.all()
        cs_fake_data = CodeSnap()
        run_id =  self.make_run_id(5)
        self.render("codesnap.html", 
                    all_reuseable = all_reuseable, 
                    codesnap = cs_fake_data,
                    run_id = run_id,
                    ref_id = "blank")

class SnapView(Handler):
    def make_run_id(self,length=5):
        random_id = util.makeSalt(length)
        if(memcache.get('%s:snap:index'%random_id)):
            return self.make_run_id(length)
        return random_id
    def get(self,key):
        c_s = CodeSnap.get_by_key_name(key)
        if( not c_s):
            return self.write(json.dumps({
                "error" : "no codesnap found"
                }))
        all_reuseable = Reuseable.all()
        run_id =  self.make_run_id(5)
        self.render("codesnap.html",
                    all_reuseable = all_reuseable,
                     codesnap = c_s, 
                     ref_id = key,
                     run_id = run_id)

class Run(Handler):
    def post(self):
        data = json.loads(self.request.body)
        data_map = {
            'poly_depend' : data['poly_depend'],
            'attr': data['attr'],
            'extend' : data['extend'],
            'index': data['index'],
            'shadow': data['shadow'] ,
            'style' : data['style'] , 
            'script': data['script']
        }
        memcache.set_multi(data,key_prefix='%s:snap:'%data["run_id"],time=10)

PAGE_RE = r'((?:[a-zA-Z]+/?)*)'
app = webapp2.WSGIApplication([
    ('/', Snap),
    ('/run/?',Run),
    ('/'+PAGE_RE, SnapView),
    #('/component/?', ImportComponent),
    #('/runtime/?uid' , RuntimeComponent)
], debug=False)
