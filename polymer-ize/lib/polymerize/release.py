from polymerize.handler import Handler 
from polymerize.db import Component,Reuseable

class Release(Handler):
	def get(self):
		cid = self.request.get('id')
		if not(self.user and cid ):
			return self.redirect('/')
		cid = int(cid)
		component = Component.get_by_id(cid)
		if (not component):
			return self.redirect('/')
		if not(self.user.key().id() == component.uid):
			return self.redirect('/')
		self.render('release.html',component=component)

	def post(self):
		cid = self.request.get('id')
		if  not(self.user and cid ):
			return self.redirect('/')
		cid = int(cid)
		component = Component.get_by_id(cid)
		if not component:
			return self.redirect('/')
		#validate data
		if not(component.uid == self.user.key().id()):
			return self.redirect('/')
		version = self.request.get('version')
		basicuse = self.request.get('basicuse')
		features = self.request.get('additional')
		whats_new = self.request.get('whatsnew') or ''
		assert version
		assert basicuse
		assert features
		if not(version and basicuse and features):
			return self.write('bad data')
		reuse = Reuseable(componentid = component.key().id(),
							element_name = component.element_name,
							element_attr = component.element_attr,
							element_extends = component.element_extends,
							polymer_imports = component.polymer_imports,
							polymer_deep = component.polymer_deep,
							polymer_shadow = component.polymer_shadow,
							polymer_style = component.polymer_style,
							polymer_script = component.polymer_script,
							index_body = component.index_body,
							#index_imports = component.index_imports,
							version = version,
							basic_use = basicuse,
							features = features,
							whats_new = whats_new,
							)
		reuse.put()
		component.publish_sync = True
		component.put()