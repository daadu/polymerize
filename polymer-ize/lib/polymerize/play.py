from polymerize.handler import Handler 
from polymerize.db import Component, Reuseable

class Play(Handler):
	DEFAULT_SHADOW = """<div>Hello this is shadow dom</div>"""
	DEFAULT_STYLE = """:host{display:block;}"""
	def get(self):
		component_id = self.request.get('id')
		if not(self.user and component_id):
			return self.redirect('/')
		component_id = int(component_id)
		component = Component.get_by_id(component_id)
		if(not component):
			return self.redirect('/')
		name = component.element_name
		self.DEFAULT_BODY = '<%(name)s></%(name)s>'%{"name" : name}
		self.DEFAULT_SCRIPT = """Polymer('%(name)s',{});"""%{"name": name}
		poly_depend = component.polymer_imports
		attr = component.element_attr
		extends = component.element_extends
		shadow = component.polymer_shadow or self.DEFAULT_SHADOW
		style = component.polymer_style or self.DEFAULT_STYLE
		script = component.polymer_script or self.DEFAULT_SCRIPT
		body = component.index_body or self.DEFAULT_BODY
		#body_depend = component.index_imports
		reuseables = Reuseable.all()
		return self.render("play.html",
							#body_depend = body_depend,
							poly_depend = poly_depend,
							reuseables = reuseables,
							comp_id = component_id,
							name = name,
							attr = attr ,
							extends = extends , 
							shadow = shadow,
							style = style,
							script = script,
							body = body)