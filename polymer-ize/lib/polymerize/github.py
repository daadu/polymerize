import logging
import simplejson as json

from polymerize.handler import Handler
from polymerize.db import User
import polymerize.util as util

from urllib import urlencode, unquote
from google.appengine.api import urlfetch

class GithubCallback(Handler):
	GET_TOKEN_URL = "https://github.com/login/oauth/access_token"
	GITHUB_CLIENT_ID = "1a423425e001aca765fe"
	GITHUB_CLIENT_SECRET = "f77c5c38dc1cb8a808e2f2bffac3ec8c05941d70"
	def get(self):
		code  = self.request.get("code")
		state = self.request.get("state")
		if not(code and state):
			return self.write(json.dumps({"error" : "parameters missing"}))
		data = {"client_id" : self.GITHUB_CLIENT_ID, "client_secret" : self.GITHUB_CLIENT_SECRET, "code" : code}
		headers = {"Accept": "application/json"}
		response=urlfetch.fetch(url=self.GET_TOKEN_URL,payload=urlencode(data),headers=headers,method="POST")
		content = json.loads(response.content)
		if content.get("error") or not content.get('access_token'):
			return self.write(json.dumps({"error" : "invalid parameters"}))
		access_token = content.get('access_token')
		scope = content.get('scope')
		if(scope != 'public_repo,user' and scope != 'user,public_repo'):
			return self.redirect('/github/resignin')
		user_details = urlfetch.fetch(url="https://api.github.com/user",
									payload='',
									headers={'Authorization':'token '+access_token},
									method="get").content
		user_details = json.loads(user_details)
		if (user_details.get('message') or not user_details.get('email')):
			return self.write(json.dumps({"error" : "not valid token"}))
		u = User.get_by_email(user_details.get('email'))
		if(u):
			cookie = util.writeLoginUser(u.key().id())
			self.response.headers.add_header("Set-Cookie","uid=%s;Domain=app.polymer-ize.appspot.com; Path=/"%(cookie))
			return self.redirect('/')
		user = User( 	email = user_details['email'],
						username = user_details['login'],
						photo = user_details['avatar_url'],
						github_token = access_token )
		user.put()
		cookie = util.writeLoginUser(user.key().id())
		self.response.headers.add_header("Set-Cookie","uid=%s;Domain=app.polymer-ize.appspot.com; Path=/"%(cookie))
		return self.redirect('/')