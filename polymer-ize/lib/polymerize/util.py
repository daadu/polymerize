import logging
import simplejson as json
import os
import random
import hmac
import hashlib
from string import letters

from google.appengine.api import memcache

from polymerize.db import Reuseable

class data:
    def __init__(self, **kw):
        for name in kw:
            setattr(self, name, kw[name])

def deep_dependencies(poly_depend):
	revtal = set(poly_depend)
	for each_depend in poly_depend:
		el = Reuseable.get_by_id(each_depend)
		revtal = revtal | set(el.polymer_deep)
	return list(revtal)

def readSecretFile():
	if os.environ.get('SERVER_SOFTWARE','').startswith('Development'):
		secret_file = file(os.path.join(os.path.dirname(__file__),'secret.json'),'r')
		secret = secret_file.read()
		secret = json.loads(secret)
		return secret
	secret_cache = memcache.get('secret')
	if(secret_cache):
		return secret_cache
	secret_file = file(os.path.join(os.path.dirname(__file__),'secret.json'),'r')
	secret = secret_file.read()
	secret = json.loads(secret)
	memcache.set('secret',secret)
	return readSecretFile()

def writeLoginUser(userid):
	secret = readSecretFile()
	key = secret['login']['key']
	return str(userid)+"||"+ hashlib.sha256(str(userid)+str(key)).hexdigest()

def verifyLoginUser(cookie):
	secret = readSecretFile()
	key = secret['login']['key']
	userid = cookie.split("||")[0]
	cookie = cookie.split("||")[1]
	#logging.error(hashlib.sha256(str(userid)+str(key)+str(email)).hexdigest() == cookie)
	return (hashlib.sha256(str(userid)+str(key)).hexdigest() == cookie)

def makeSalt(length=5):
	s=""
	for i in range(length):
		s += random.choice(letters)
	return s