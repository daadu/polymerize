from polymerize.handler import Handler
from polymerize.db import Component
import polymerize.util as util

class CreateElement(Handler):
	def post(self):
		if not(self.user):
			return self.redirect('/')

		dependecnies = self.request.params.getall('imports') or []
		dependecnies = map(int, dependecnies)

		all_dependencies = util.deep_dependencies(dependecnies)
		name = self.request.get('name')
		if not name:
			return self.write('enter element name')
		element = Component(
				uid = self.user.key().id() ,
				element_name = name , 
				polymer_imports = dependecnies ,
				polymer_deep = all_dependencies,
			)
		element.put()
		return self.redirect('/play?id=%d'% element.key().id() )