from polymerize.handler import Handler
from polymerize.db import Component, Reuseable

class Home(Handler):
	def get(self):
		if(self.user):
			elements = Reuseable.all()
			custom = Component.all().filter('uid =',self.user.key().id())
			return self.render("main.html",elements=elements,my_elements = custom)
		self.render("home.html")