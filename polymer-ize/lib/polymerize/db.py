from google.appengine.ext import db

class User(db.Model):
	email = db.EmailProperty( required = True )
	username = db.TextProperty( required = True )
	photo = db.LinkProperty( required = True )
	github_token = db.StringProperty( required = True )

	@classmethod
	def get_by_email(cls,token):
		u = cls.all().filter('email =',token).get()
		return u

class Component(db.Model):
	uid = db.IntegerProperty( required = True )
	element_name = db.StringProperty( required = True )
	element_attr = db.StringProperty( default= "" )
	element_extends = db.StringProperty( default = "" )
	polymer_imports = db.ListProperty( item_type = long , default = [] )
	polymer_deep = db.ListProperty( item_type = long , default = [] )
	polymer_shadow = db.TextProperty( default =  "" )
	polymer_style = db.TextProperty( default = "" )
	polymer_script = db.TextProperty( default = "" )
	index_body = db.TextProperty( default = "" ) 
	#index_imports = db.ListProperty( item_type = long , default = [] )
	created_time = db.DateTimeProperty(auto_now_add = True)
	last_modified = db.DateTimeProperty( auto_now = True )
	publish_sync = db.BooleanProperty( default = False )

class Reuseable(db.Model):
	componentid = db.IntegerProperty( required = True )
	element_name = db.StringProperty( required = True )
	element_attr = db.StringProperty( default = '' )
	element_extends = db.StringProperty( default = '')
	polymer_imports = db.ListProperty( item_type = long, default = [] )
	polymer_deep =  db.ListProperty( item_type = long, default = [] )
	polymer_shadow = db.TextProperty( required = True )
	polymer_style = db.TextProperty( required = True )
	polymer_script = db.TextProperty( required = True )
	index_body = db.TextProperty( required = True )
	#index_imports = db.ListProperty( item_type = long, default = [] )
	created_time = db.DateTimeProperty(auto_now_add = True)
	version = db.StringProperty( default = '1.0' )
	basic_use = db.TextProperty( required = True )
	features = db.TextProperty( required = True )
	whats_new = db.TextProperty()

class CodeSnap(db.Model):
	componentid = db.IntegerProperty()
	refrenceid = db.StringProperty()
	element_name = db.StringProperty(default = "my-element")
	element_extends = db.StringProperty( default = "")
	element_attr = db.StringProperty( default = "")
	polymer_imports = db.ListProperty( item_type = long, default = [] )
	polymer_deep = db.ListProperty( item_type = long, default = [] )
	polymer_shadow = db.TextProperty( default =  "<div>this is shadow dom</div>" )
	polymer_style = db.TextProperty(default =  """:host{
    display: block;
}""" )
	polymer_script = db.TextProperty(default = """Polymer('my-element',{
    
}); """)
	index_body = db.TextProperty( default = "<my-element></my-element>" )
	created_time = db.DateTimeProperty( auto_now_add = True )