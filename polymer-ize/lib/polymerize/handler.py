import webapp2
import jinja2
import os
import logging

import polymerize.util as util
from polymerize.db import User

def custom_escape(chunk,*a):
    chunk = chunk or ''
    a = a or []
    all_map = {
        '<' : '&lt;',
        '>' : '&gt;',
        "'" : '&#39;',
        '"' : '&#34;',
        '{' : '&#123;',
        '}' : '&#125;',
    }
    if not a :
        a= ['&','<','>',"'",'"','{','}']
    if '&' in a:
        chunk = chunk.replace('&','&amp;')
        a.remove('&')
    for each in a:
        chunk = chunk.replace(each,all_map.get(each))
    return chunk

def escape_curly(chunk):
    chunk = chunk  or ''
    chunk = chunk.replace('{','&#123;')
    chunk = chunk.replace('}','&#125;')
    return chunk
def remove_amp(chunk):
    chunk = chunk or ''
    chunk = chunk.replace('&amp;#123;','&#123;')
    chunk = chunk.replace('&amp;#125;','&#125;')
    return chunk
def escape_angle(chunk):
    chunk = chunk or ''
    chunk = chunk.replace('<','&lt;')
    chunk = chunk.replace('>','&gt;')
    return chunk

template_dir = os.path.join(os.path.dirname(__file__),'..','..','templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir),
                                autoescape=True)
                                #extensions=['jinja2.ext.autoescape'])
jinja_env.filters['escape_curly'] = escape_curly
jinja_env.filters['remove_amp'] = remove_amp
jinja_env.filters['escape_angle'] = escape_angle
jinja_env.filters['custom_escape'] = custom_escape

class Handler(webapp2.RequestHandler):
    def write(self,*a,**kw):
        self.response.out.write(*a,**kw)
    def render_str(self,template,**params):
        t=jinja_env.get_template(template)
        return t.render(params)
    def render(self, template,**kw):
        self.write(self.render_str(template,**kw))

    def initialize(self, *a, **kw):
	webapp2.RequestHandler.initialize(self,*a,**kw)
        uid = self.request.cookies.get('uid')
        self.user = None
        if(uid):
            userid = int(uid.split("||")[0])
            if(util.verifyLoginUser(uid)):
                self.user = User.get_by_id(userid)