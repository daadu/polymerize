import simplejson as json

from polymerize.handler import Handler
from polymerize.db import Component
import polymerize.util as util

from google.appengine.api import memcache

class Save(Handler):
	def post(self):
		if not(self.user):
			raise Exception, "Not logged in"
			return
		data = json.loads(self.request.body)
		uid = self.user.key().id()
		data_map = {
			'comp_id' : data['comp_id'] ,
			#'body_depend' : data['body_depend'],
			'poly_depend' : data['poly_depend'],
			'attr': data['attr'],
			'extend' : data['extend'],
			'index': data['index'],
			'shadow': data['shadow'] ,
			'style' : data['style'] , 
			'script': data['script']
		}
		comp = Component.get_by_id(int(data_map.get('comp_id')))
		if not comp:
			raise Exception, "Not vaild comp_id"
			return
		if not(comp.uid == uid):
			raise Exception, "Not your Component"
			return
		comp.element_attr = data['attr']
		#comp.index_imports = map(long,data['body_depend'])
		comp.polymer_imports = map(long,data['poly_depend'])
		comp.polymer_deep = util.deep_dependencies(comp.polymer_imports)
		comp.element_extends = data['extend']
		comp.index_body = data['index']
		comp.polymer_shadow = data['shadow']
		comp.polymer_style = data['style']
		comp.polymer_script = data['script']
		comp.publish_sync = False
		comp.put()

		memcache.set_multi(data,key_prefix='%d:run:'%uid,time=10)