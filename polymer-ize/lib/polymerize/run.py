import simplejson as json
import logging

from polymerize.handler import Handler
from polymerize.db import Component

from google.appengine.api import memcache

class Run(Handler):
	def post(self):
		if not(self.user):
			raise Exception, "Not logged in"
			return
		data = json.loads(self.request.body)
		uid = self.user.key().id()
		data_map = {
			'comp_id' : data['comp_id'] ,
			#'body_depend' : data['body_depend'],
			'poly_depend' : data['poly_depend'],
			'attr': data['attr'],
			'extend' : data['extend'],
			'index': data['index'],
			'shadow': data['shadow'] ,
			'style' : data['style'] , 
			'script': data['script']
		}
		#logging.error(data_map)
		comp = Component.get_by_id(int(data_map.get('comp_id')))
		if not comp:
			raise Exception, "Not vaild comp_id"
			return
		if not(comp.uid == uid):
			raise Exception, "Not your Component"
			return
		# 	'run:attr:%d'%uid : data['attr'] ,
		# 	'run:ext:%d'%uid  : data['extends'] ,
		# 	'run:body:%d'%uid : data['index'] ,
		# 	'run:shad:%d'%uid : data['shadow'] ,
		# 	'run:styl:%d'%uid : data['style'] , 
		# 	'run:scri:%d'%uid : data['script'] , 
		# }
		memcache.set_multi(data,key_prefix='%d:run:'%uid,time=30)