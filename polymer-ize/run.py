import webapp2
import os
import logging
import sys

sys.path.insert(0, 'lib')

import simplejson as json

from polymerize.handler import Handler
from polymerize.db import Component, Reuseable
import polymerize.util as util

from google.appengine.api import memcache

class View(Handler):
    def get(self):
        uid =  self.request.get('uid')
        if(not uid):
        	raise Exception, "Invalid params"
        	return
        keys = ["index","comp_id"]
        data_map = memcache.get_multi(keys,key_prefix='%s:run:'%uid)
        if not data_map:
        	return self.write(json.dumps({"error" : "latency more than 5000ms"}))
        #component = Component.get_by_id(int(data_map['comp_id']))
        self.render('view.html',data=data_map,uid=uid)

class ImportComponent(Handler):
    def get(self):
        #cid = self.request.get('cid')
        uid = self.request.get('uid')
        if not (uid):
            raise Exception, 'Invalid params'
            return
        #cid = int(cid)
        uid = int(uid)
        cid = memcache.get('%d:run:comp_id'%uid)
        if( not cid):
            raise Exception, 'Invalid params'
            return
        cid  = int(cid)
        component = Component.get_by_id(cid)
        if not component:
            raise Exception, 'No component found'
            return
        #body_depend = memcache.get('%d:run:body_depend'%uid)
        poly_depend = memcache.get("%d:run:poly_depend"%uid)
        all_dependencies = util.deep_dependencies(poly_depend)
        all_dependencies = map(lambda x: Reuseable.get_by_id(x), all_dependencies )

        polymer_data_key = ['attr',"extend","shadow","style","script"]
        polymer_data_map = memcache.get_multi(polymer_data_key,key_prefix='%d:run:'%uid)
        polymer_data_map['name'] = component.element_name
        return self.render("imports.html",all_dependencies = all_dependencies,polymer=polymer_data_map)

class SnapRun(Handler):
    def get(self):
        run_id = self.request.get('id')
        if not run_id:
            raise Exception, "invalid params"
            return
        get_keys  = ['index']
        data_map =  memcache.get_multi(get_keys,key_prefix="%s:snap:"%run_id)
        #get_keys = ['poly_depend','attr','extend','index','shadow','style','script']
        self.render("view.html",run_id=run_id,data=data_map)

class SnapImport(Handler):
    def get(self):
        run_id = self.request.get('id')
        if not run_id:
            raise Exception, 'invalid params'
            return
        get_keys = ['name','poly_depend','attr','extend','shadow','style','script']
        data_map  = memcache.get_multi(get_keys,key_prefix="%s:snap:"%run_id)
        if( not data_map):
            raise Exception, 'data not found'
            return
        all_dependencies = util.deep_dependencies(map(long, data_map["poly_depend"]))
        all_dependencies = map(lambda x: Reuseable.get_by_id(x), all_dependencies )
        polymer = util.data(
                        name = data_map["name"],
                        attr = data_map["attr"],
                        extend = data_map["extend"],
                        style = data_map["style"],
                        shadow = data_map["shadow"],
                        script = data_map["script"])
        self.render('imports.html',
                    all_dependencies = all_dependencies,
                    polymer = polymer
                    )

app = webapp2.WSGIApplication([
    ('/', View),
    ('/snap/?', SnapRun),
    ('/component/snap/?', SnapImport),
    ('/component/?', ImportComponent),
    #('/runtime/?uid' , RuntimeComponent)
], debug=False)
