#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2
import os
import logging
import sys
sys.path.insert(0, 'lib')

from polymerize.handler import Handler
from polymerize.home import Home
#from polymerize.submit import Submit
#from polymerize.view import View
#from polymerize.component import Component
from polymerize.github import GithubCallback
from polymerize.element import CreateElement
from polymerize.play import Play
from polymerize.run import Run
from polymerize.save import Save
from polymerize.release import Release

app = webapp2.WSGIApplication([
    ( '/' , Home ) ,
    #( '/submit/?' , Submit ) ,
    #( '/view/?' , View ) ,
    ( '/play/?', Play ) ,
    ( '/run/?' , Run ) ,
    ( '/save/?' , Save ) ,
    ( '/release/?' , Release ) ,
    ( '/create_element/?' , CreateElement ) ,
    #( '/component/?', Component ) ,
    ( '/github/callback/?', GithubCallback )
], debug=False)
